package com.example.danco.homework2.activity;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
@RunWith(AndroidJUnit4.class)
public class WeatherActivityTest extends ActivityInstrumentationTestCase2<WeatherActivity> {

    private WeatherActivity weatherActivity;

    /**
     * Creates an {@link ActivityInstrumentationTestCase2}.
     *
     * @param activityClass The activity to test. This must be a class in the instrumentation
     *                      targetPackage specified in the AndroidManifest.xml
     */
    public WeatherActivityTest(Class<WeatherActivity> activityClass) {
        super(activityClass);
    }


    @Before
    public void setUp() throws Exception{
        super.setUp();

        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        weatherActivity = getActivity();
    }

    @Test
    public void test() {

    }
}
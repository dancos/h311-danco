package com.example.danco.homework2.event;

/**
 * @author danco on 5/24/15.
 */
public class WeatherUpdateEvent {
    private long cityId;
    private String cityName;


    public WeatherUpdateEvent(long cityId, String cityName) {
        this.cityId = cityId;
        this.cityName = cityName;
    }


    public long getCityId() {
        return cityId;
    }


    public void setCityId(long cityId) {
        this.cityId = cityId;
    }


    public String getCityName() {
        return cityName;
    }


    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}

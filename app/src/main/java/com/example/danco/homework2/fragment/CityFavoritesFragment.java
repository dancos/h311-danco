package com.example.danco.homework2.fragment;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.danco.R;
import com.example.danco.homework2.adapter.CityFavoritesAdapter;
import com.example.danco.homework2.loader.CityFavoritesLoaderCallbacks;
import com.example.danco.homework2.provider.CityContract;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;

/**
 * @author costd037 on 5/21/15.
 */
public class CityFavoritesFragment extends Fragment implements
        CityFavoritesLoaderCallbacks.OnCityFavoritesLoaded,
        AdapterView.OnItemClickListener,
        AdapterView.OnItemLongClickListener {

    private CityFavoritesFragmentListener listener;
    private long cityId;
    private CityFavoritesAdapter adapter;

    public static CityFavoritesFragment newInstance() {
        return new CityFavoritesFragment();
    }


    public interface CityFavoritesFragmentListener {
        void onFavoriteSelected(String cityName, long cityId);

        void onFavoriteLongClick(String cityName, long cityId, long id);
    }


    public CityFavoritesFragment() {
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Fragment parent = getParentFragment();
        Object objectToCast = parent != null ? parent : activity;
        try {
            listener = (CityFavoritesFragmentListener) objectToCast;
        } catch (ClassCastException e) {
            throw new ClassCastException(objectToCast.getClass().getSimpleName()
                    + " must implement CityFavoritesFragmentListener");
        }
    }


    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view
     *                           itself, but this can be used to generate the LayoutParams of the
     *                           view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_city_favorites, container, false);
    }


    /**
     * Called immediately after {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * has returned, but before any saved state has been restored in to the view.
     * This gives subclasses a chance to initialize themselves once
     * they know their view hierarchy has been completely created.  The fragment's
     * view hierarchy is not however attached to its parent at this point.
     *
     * @param view               The View returned by {@link #onCreateView(LayoutInflater,
     * ViewGroup, Bundle)}.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ListView favorites = (ListView) view.findViewById(R.id.city_favorites_list);
        adapter = new CityFavoritesAdapter(favorites.getContext());
        favorites.setAdapter(adapter);
        favorites.setOnItemClickListener(this);
        favorites.setOnItemLongClickListener(this);

        favorites.setEmptyView(view.findViewById(R.id.empty));
    }


    /**
     * Called when the fragment's activity has been created and this
     * fragment's view hierarchy instantiated.  It can be used to do final
     * initialization once these pieces are in place, such as retrieving
     * views or restoring state.  It is also useful for fragments that use
     * {@link #setRetainInstance(boolean)} to retain their instance,
     * as this callback tells the fragment when it is fully associated with
     * the new activity instance.  This is called after {@link #onCreateView}
     * and before {@link #onViewStateRestored(Bundle)}.
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        CityFavoritesLoaderCallbacks.initLoader(
                getLoaderManager(), getActivity(), this, adapter.PROJECTION);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String cityName = adapter.getCityName(position);
        long cityId = adapter.getCityId(position);
        listener.onFavoriteSelected(cityName, cityId);
    }


    /**
     * Callback method to be invoked when an item in this view has been
     * clicked and held.
     * <p/>
     * Implementers can call getItemAtPosition(position) if they need to access
     * the data associated with the selected item.
     *
     * @param parent   The AbsListView where the click happened
     * @param view     The view within the AbsListView that was clicked
     * @param position The position of the view in the list
     * @param id       The row id of the item that was clicked
     * @return true if the callback consumed the long click, false otherwise
     */
    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        String cityName = adapter.getCityName(position);
        long cityId = adapter.getCityId(position);
        listener.onFavoriteLongClick(cityName, cityId, id);
        return false;
    }


    @Override
    public void onCityFavoritesLoaded(Cursor cursor) {
        if (adapter != null) {
            adapter.swapCursor(cursor);
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        adapter.swapCursor(null);
        adapter = null;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}

package com.example.danco.homework2.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;

import com.example.danco.homework2.application.WeatherApplication;
import com.example.danco.homework2.event.WeatherUpdateEvent;
import com.example.danco.homework2.service.WeatherService;
import com.squareup.otto.Bus;

import java.lang.ref.WeakReference;

public class CityAddedReceiver extends BroadcastReceiver {
    public static final String ACTION_CITY_ADDED = "city_added";
    private static final String EXTRA_CITY_ID = "cityId";
    private static final String EXTRA_CITY_NAME = "cityName";

    private WeakReference<CityAddedListener> listenerRef;


    public interface CityAddedListener {
        void onCityAdded(long cityId, String cityName);
    }


    public CityAddedReceiver() {
    }


    public CityAddedReceiver(CityAddedListener listener) {
        listenerRef = new WeakReference<CityAddedListener>(listener);
    }


    @Override
    public void onReceive(final Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(ACTION_CITY_ADDED)) {
            final long cityId = intent.getLongExtra(EXTRA_CITY_ID, 0);
            final String cityName = intent.getStringExtra(EXTRA_CITY_NAME);
            WeatherService.startActionReLoad(context, cityId, cityName);

            CityAddedListener listener = listenerRef == null ? null : listenerRef.get();

            if (listener == null) {
                final Bus bus = ((WeatherApplication) context.getApplicationContext()).getBus();
                final WeatherUpdateEvent event = new WeatherUpdateEvent(cityId, cityName);
                bus.post(event);
            } else {
                listener.onCityAdded(cityId, cityName);
            }
        }
    }
}

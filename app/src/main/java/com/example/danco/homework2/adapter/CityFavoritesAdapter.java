package com.example.danco.homework2.adapter;

import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.danco.R;
import com.example.danco.homework2.provider.CityContract;

/**
 * @author costd037 on 5/21/15.
 */
public class CityFavoritesAdapter extends CursorAdapter {

    public static final String[] PROJECTION = new String[] {
            CityContract.Columns._ID,
            CityContract.Columns.UPDATED,
            CityContract.Columns.CITY_ID,
            CityContract.Columns.CITY_NAME
    };

    private static final int ID_POS = 0;
    private static final int UPDATED_POS = 1;
    private static final int CITY_ID_POS = 2;
    private static final int CITY_NAME_POS = 3;

    /**
     * Constructor that always enables auto-requery.
     *
     * @param context The context
     */
    public CityFavoritesAdapter(Context context) {
        super(context, null, false);
    }


    public long getCityId(int position) {
        Cursor cursor = getCursor();
        if (cursor != null && cursor.moveToPosition(position)) {
            return cursor.getLong(CITY_ID_POS);
        }
        return 0;
    }


    public String getCityName(int position) {
        Cursor cursor = getCursor();
        if (cursor != null && cursor.moveToPosition(position)) {
            return cursor.getString(CITY_NAME_POS);
        }
        return "";
    }


    /**
     * Makes a new view to hold the data pointed to by cursor.
     *
     * @param context Interface to application's global information
     * @param cursor  The cursor from which to get the data. The cursor is already
     *                moved to the correct position.
     * @param parent  The parent to which the new view is attached to
     * @return the newly created view.
     */
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_city_name_item,
                parent, false);
        view.setTag(new ViewHolder(view));
        return view;
    }

    /**
     * Bind an existing view to the data pointed to by cursor
     *
     * @param view    Existing view, returned earlier by newView
     * @param context Interface to application's global information
     * @param cursor  The cursor from which to get the data. The cursor is already
     */
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();

        holder.cityId.setText(cursor.getString(CITY_ID_POS));
        holder.cityName.setText(cursor.getString(CITY_NAME_POS));
    }


    /* package */ static class ViewHolder {
        final TextView cityId;
        final TextView cityName;
        final TextView empty;

        ViewHolder(View view) {
            empty = (TextView) view.findViewById(R.id.empty);
            cityId = (TextView) view.findViewById(R.id.cityId);
            cityId.setVisibility(View.GONE);
            cityName = (TextView) view.findViewById(R.id.cityName);
        }
    }

}

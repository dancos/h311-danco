package com.example.danco.homework2.loader;

/**
 * Created by danco on 4/18/15.
 */
public final class LoaderIds {
    public static final int CITY_CONDITIONS_ID = 100;
    public static final int DAILY_FORECAST_ID = 101;
    public static final int FORECAST_ID = 102;
    public static final int CITY_FAVORITES_ID = 103;
}

package com.example.danco.homework2.service;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import com.example.danco.homework2.network.CacheUtils;
import com.example.danco.homework2.network.CityListJsonParser;
import com.example.danco.homework2.network.ConnectivityUtils;
import com.example.danco.homework2.network.DataFetcher;
import com.example.danco.homework2.provider.CityContract;

import java.net.URL;

/**
 * @author danco on 5/17/15.
 */
public class FetchCityHelper {
    private static final String TAG = "CitySelectorHelper";


    public interface DelayAndRetryHandler {
        public void delayAndRetry();

        public void delayAndRetryError();

        public boolean isLoadCancelled();
    }


    public static void loadData(Context context, DelayAndRetryHandler handler,
                                double lat, double lng) {
        if (!DataFetcher.ensureLatestSSL(context)) {
            return;
        }

        CacheUtils.initializeCache(context);

        // No network, we should get out
        if (ConnectivityUtils.isNotConnected(context)) {
            handler.delayAndRetryError();
            return;
        }

        try {
            URL url = CitySelectorService.buildCitySelectionURL(lat, lng);
            if (handler.isLoadCancelled()) {
                return;
            }
            ContentValues[] values = DataFetcher.performGet(url, new CityListJsonParser());
            if (values != null) {
                if (handler.isLoadCancelled()) {
                    return;
                }
                int rows = context.getContentResolver().bulkInsert(
                        CityContract.URI, values);
                Log.d(TAG, "Inserted rows: " + rows);

                if (handler.isLoadCancelled()) {
                    return;
                }
            }
        } catch (Exception e) {
            Log.w(TAG, "Unexpected error", e);
        }

        CacheUtils.logCache();
    }
}

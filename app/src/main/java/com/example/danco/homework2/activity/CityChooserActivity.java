package com.example.danco.homework2.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.danco.R;
import com.example.danco.homework2.adapter.PlaceItem;
import com.example.danco.homework2.fragment.CityChooserFragment;
import com.example.danco.homework2.fragment.PlayServicesDialogFragment;
import com.example.danco.homework2.network.JobUtils;
import com.example.danco.homework2.service.CitySelectorJobService;
import com.example.danco.homework2.service.CitySelectorService;
import com.example.danco.homework2.service.FetchCityHelper;
import com.example.danco.homework2.service.WeatherService;

public class CityChooserActivity extends AppCompatActivity implements
        PlayServicesDialogFragment.PlayServicesDialogFragmentListener,
        CityChooserFragment.CityChooserFragmentListener {

    private static final String CITY_CHOOSER_FRAG = "CityChooserFrag";
    private static final String CITY_FAVORITES_FRAG = "CityFavoritesFrag";

    public static Intent buildIntent(Context context) {
        return new Intent(context, CityChooserActivity.class);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_chooser);
        setSupportActionBar((Toolbar) findViewById(R.id.toolBar));
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                Intent intent = NavUtils.getParentActivityIntent(this);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                NavUtils.navigateUpTo(this, intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        // Uncomment this if, if you want to test using the activity check. However,
        // comment out the places fragment onConnectionFailed implementation or you
        // are showing two dialogs...

        //if (isGooglePlayServicesReady()) {
        //    Log.d(TAG, "Google play services available");
        // If need GPS here, you could start your "connection". Alternatively,
        // just connect to the api and handle the error that way. See
        // http://developer.android.com/google/auth/api-client.html
        // I tend to check up front in the app, unless I can successfully defer to a later
        // activity. This allows the app to avoid doing all of the error handling in
        // different fragments/services and I would put up a simple notification if
        // somehow it failed. This is highly unlikely unless the user manually uninstalls
        // the updates for google play services.
        //}
    }


    @Override
    public void onCitySelected(PlaceItem selected) {
        //City selected, update CITY_DB, WeatherActivity will use new value when it updates,
        // which should happen in the onResume() when it calls startActionLoad
        Log.d("CityChooserActivity", "onCitySelected method called with " +
                selected.getLatLng().toString());

        //this service should broadcast when it's complete, implement a bcast recvr to respond to
        // that event and restart the conditions loader?
        CitySelectorService.startActionLoadCity(this,
                selected.getLatLng().latitude,
                selected.getLatLng().longitude);

        setResult(Activity.RESULT_OK);
        finish();
    }


    @Override
    public void noPlayServicesAvailable() {
        Toast.makeText(this, R.string.play_services_error,
                Toast.LENGTH_LONG).show();
        finish();
    }


    @Override
    public void onDialogCancelled() {

    }
}

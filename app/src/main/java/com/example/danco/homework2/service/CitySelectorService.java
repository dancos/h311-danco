package com.example.danco.homework2.service;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.danco.BuildConfig;
import com.example.danco.R;
import com.example.danco.homework2.fragment.ConditionsFragment;
import com.example.danco.homework2.network.CacheUtils;
import com.example.danco.homework2.network.CityListJsonParser;
import com.example.danco.homework2.network.DataFetcher;
import com.example.danco.homework2.provider.BaseContract;
import com.example.danco.homework2.provider.CityConditionsContract;
import com.example.danco.homework2.provider.CityContract;
import com.example.danco.homework2.provider.ForecastContract;
import com.example.danco.homework2.receiver.CityAddedReceiver;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author danco on 5/19/15.
 */
public class CitySelectorService extends IntentService {
    private static final String LOG_TAG = CitySelectorService.class.getSimpleName();

    private static final String EXTRA_LATITUDE =
            BuildConfig.APPLICATION_ID + ".service.extra.LATITUDE";
    private static final String EXTRA_LONGITUDE =
            BuildConfig.APPLICATION_ID + ".service.extra.LONGITUDE";
    private static final String EXTRA_CITY_NAME = "cityName";
    private static final String EXTRA_CITY_ID = "cityId";


    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionLoadCity(Context context, double latitude, double longitude) {
        Intent intent = new Intent(context, CitySelectorService.class);
        intent.putExtra(EXTRA_LATITUDE, latitude);
        intent.putExtra(EXTRA_LONGITUDE, longitude);
        context.startService(intent);
    }


    public CitySelectorService() {
        super("CitySelectorService");
    }


    public static URL buildCitySelectionURL(final double lat, final double lon) {
        Uri.Builder builder = new Uri.Builder()
                .scheme("http")
                .authority(CitySelectorJobService.WEATHER_API_PROVIDER)
                .appendPath("data")
                .appendPath("2.5")
                .appendPath("find")
                .appendQueryParameter("lat", Double.toString(((double)(int)(lat * 1000) / 1000)))
                .appendQueryParameter("lon", Double.toString(((double)(int)(lon * 1000) / 1000)))
                .appendQueryParameter("cnt", Long.toString(1));

        Uri uri = builder.build();

        try {
            return new URL(uri.toString());
        } catch (MalformedURLException e) {
            Log.w(LOG_TAG, "Failed to build URL: " + uri.toString());
            return null;
        }
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            double lat = intent.getDoubleExtra(EXTRA_LATITUDE, 0.0);
            double lng = intent.getDoubleExtra(EXTRA_LONGITUDE, 0.0);

            handleActionLoadCity(lat, lng);
        }
    }


    private void handleActionLoadCity(double lat, double lng) {
        CacheUtils.initializeCache(this);

        if (!DataFetcher.ensureLatestSSL(this)) {
            return;
        }

        URL url = buildCitySelectionURL(lat, lng);
        Log.d(LOG_TAG, url.toString());
        ContentValues[] values = DataFetcher.performGet(url, new CityListJsonParser());
        long cityId = 0;
        String cityName = "";
        int rows = 0;
        if (values != null) {
            cityId = values[0].getAsLong(BaseContract.BaseWeatherColumns.CITY_ID);
            cityName = values[0].getAsString(BaseContract.BaseWeatherColumns.CITY_NAME);
            rows = getContentResolver().bulkInsert(CityContract.URI, values);
        }

        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, "Inserted " + rows + " Rows");
            CacheUtils.logCache();
        }

        Intent intent = new Intent(CityAddedReceiver.ACTION_CITY_ADDED);
        intent.putExtra(EXTRA_CITY_NAME, cityName);
        intent.putExtra(EXTRA_CITY_ID, cityId);

        IntentFilter filter = new IntentFilter();
        filter.addAction(CityAddedReceiver.ACTION_CITY_ADDED);

        if (getResources().getBoolean(R.bool.use_otto_bus)) {
            LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(
                    new CityAddedReceiver(), filter);
        }
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

}

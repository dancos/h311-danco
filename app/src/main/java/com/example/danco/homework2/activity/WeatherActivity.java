package com.example.danco.homework2.activity;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.danco.R;
import com.example.danco.homework2.application.WeatherApplication;
import com.example.danco.homework2.event.WeatherUpdateEvent;
import com.example.danco.homework2.fragment.CityChangedListener;
import com.example.danco.homework2.fragment.ConditionsFragment;
import com.example.danco.homework2.fragment.DailyForecastFragment;
import com.example.danco.homework2.network.AlarmUtils;
import com.example.danco.homework2.network.JobUtils;
import com.example.danco.homework2.provider.BaseContract;
import com.example.danco.homework2.provider.CityContract;
import com.example.danco.homework2.receiver.CityAddedReceiver;
import com.example.danco.homework2.service.WeatherService;
import com.example.danco.homework2.utils.LocationUtils;
import com.squareup.otto.Subscribe;

import java.util.HashMap;
import java.util.Map;

public class WeatherActivity extends AppCompatActivity
        implements DailyForecastFragment.DailyForecastFragmentListener,
        CityChangedListener,
        CityAddedReceiver.CityAddedListener {
    private static final String LOG_TAG = WeatherActivity.class.getSimpleName();

    private static final String[] CITY_INFO = new String[] {
            BaseContract.BaseWeatherColumns.UPDATED,
            BaseContract.BaseWeatherColumns.CITY_ID,
            BaseContract.BaseWeatherColumns.CITY_NAME
    };
    private static final String CITY_ID = "city_id";
    private static final String CITY_NAME = "city_name";
    private static final String EXTRA_CITY_NAME = "cityName";
    private static final String EXTRA_CITY_ID = "cityId";

    private static final int CHOOSE_CITY_CODE = 100;
    private static final int ADD_FAVORITE_CODE = 200;
    private static final int FORECAST_DETAIL_CODE = 300;

    private String cityName;
    private long cityId;

    private WeatherApplication weatherApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        weatherApp = (WeatherApplication) getApplicationContext();
        setContentView(R.layout.activity_weather);

        setSupportActionBar((Toolbar) findViewById(R.id.toolBar));
        Location location = weatherApp.getLastLocation();

        Map<Long, String> info = getCityInfo();
        if (info != null && !info.isEmpty()) {
            cityId = info.keySet().toArray(new Long[1])[0];
            cityName = info.get(cityId);
        }

        if (savedInstanceState != null) {
            cityName = savedInstanceState.getString(CITY_NAME);
            cityId = savedInstanceState.getLong(CITY_ID);
        } else if (location != null) {
            LocationUtils.loadWeatherForLocation(this, location);
        }

        if (location == null) {
            init(cityName, cityId);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //should maybe wrap this in a scheduleUpdates method...
            JobUtils.scheduleCityConditionsUpdate(this);
            JobUtils.scheduleDailyForecastUpdate(this);
            JobUtils.scheduleForecastUpdate(this);
        } else {
            AlarmUtils.scheduleRecurringAlarm(this);
        }
    }


    public void init(final String cityName, final long cityId) {
        //noinspection ConstantConditions
        getSupportActionBar().setTitle(cityName);

        ConditionsFragment conditionsFragment = (ConditionsFragment)
                getSupportFragmentManager().findFragmentById(R.id.conditions_fragment);
        conditionsFragment.setCityId(cityId);

        DailyForecastFragment dailyForecastFragment = (DailyForecastFragment)
                getSupportFragmentManager().findFragmentById(R.id.daily_forecast_fragment);
        dailyForecastFragment.setCityId(cityId);
    }


    @Subscribe
    public void updateWeather(WeatherUpdateEvent event) {
        Log.i(LOG_TAG, "received weather event for cityId: " + event.getCityId());
        init(event.getCityName(), event.getCityId());
    }


    @Override
    public void onCityAdded(long cityId, String cityName) {
        Log.i(LOG_TAG, "received weather event for cityId: " + cityId);
        init(cityName, cityId);
    }


    @Override
    protected void onResume() {
        super.onResume();
        WeatherService.startActionLoad(this, cityId);

        if (getResources().getBoolean(R.bool.use_otto_bus)) {
            weatherApp.getBus().register(this);
        } else {
        /*
            alternate registration without using Otto
         */
            IntentFilter filter = new IntentFilter();
            filter.addAction(CityAddedReceiver.ACTION_CITY_ADDED);
            CityAddedReceiver receiver = new CityAddedReceiver(this);
            LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(
                    receiver, filter);
        }
    }


    /**
     * Dispatch onPause() to fragments.
     */
    @Override
    protected void onPause() {
        super.onPause();
        if (getResources().getBoolean(R.bool.use_otto_bus)) {
            weatherApp.getBus().unregister(this);
        } else {
        /*
            alternate registration without using Otto
         */
            CityAddedReceiver receiver = new CityAddedReceiver(this);
            LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(
                    receiver);
        }

    }

    @Override
    public void onDaySelected(long cityId, long day) {
        startActivityForResult(ForecastDetailActivity.buildIntent(this, cityId, cityName, day),
                FORECAST_DETAIL_CODE);
    }


    @Override
    public void onCityChanged(final String cityName, final long cityId) {
        this.cityName = cityName;
        this.cityId = cityId;
        init(cityName, cityId);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(CITY_NAME, cityName);
        outState.putLong(CITY_ID, cityId);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            cityName = savedInstanceState.getString(CITY_NAME);
            cityId = savedInstanceState.getLong(CITY_ID);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_weather, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_license) {
            startActivity(LicenseActivity.buildIntent(this));
            return true;
        } else if (id == R.id.action_choose_city) {
            startActivityForResult(CityChooserActivity.buildIntent(this), ADD_FAVORITE_CODE);
            return true;
        } else if (id == R.id.action_favorites) {
            startActivityForResult(CityFavoritesActivity.buildIntent(this), CHOOSE_CITY_CODE);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * Dispatch incoming result.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if ((requestCode == CHOOSE_CITY_CODE || resultCode == FORECAST_DETAIL_CODE) &&
                    data != null) {
                cityName = data.getStringExtra(EXTRA_CITY_NAME);
                cityId = data.getLongExtra(EXTRA_CITY_ID, 0);
                onCityChanged(cityName, cityId);
            } else if (requestCode == ADD_FAVORITE_CODE) {
                Map<Long, String> info = getCityInfo();
                if (info != null && !info.isEmpty()) {
                    cityId = info.keySet().toArray(new Long[1])[0];
                    cityName = info.get(cityId);
                    onCityChanged(cityName, cityId);
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private Map<Long, String> getCityInfo() {
        Cursor cursor = null;
        Map<Long, String> cityInfo = null;

        try {
            cursor = getContentResolver()
                    .query(CityContract.URI, CITY_INFO, null, null, CityContract.Columns.UPDATED);

            cityInfo = new HashMap<>(1);
            if (cursor.moveToFirst()) {
                long cityId = cursor.isNull(1) ? 0 : cursor.getLong(1);
                String cityName = cursor.isNull(2) ? "" : cursor.getString(2);

                cityInfo.put(cityId, cityName);
            } else {
                // default values -- couldn't get current location...
                cityInfo.put(5809844L, "Seattle");
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return cityInfo;
    }
}

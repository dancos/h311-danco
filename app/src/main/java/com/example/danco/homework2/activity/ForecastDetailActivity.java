package com.example.danco.homework2.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.danco.BuildConfig;
import com.example.danco.R;
import com.example.danco.homework2.fragment.ForecastFragment;

public class ForecastDetailActivity extends AppCompatActivity {

    private static final String EXTRA_CITY_ID = BuildConfig.APPLICATION_ID +
            ".activity.ForecastDetailActivity.extra.cityId";
    private static final String EXTRA_CITY_NAME = BuildConfig.APPLICATION_ID +
            ".activity.ForecastDetailActivity.extra.cityName";
    private static final String EXTRA_DAY = BuildConfig.APPLICATION_ID +
            ".activity.ForecastDetailActivity.extra.day";


    public static Intent buildIntent(Context context, long cityId, String cityName, long day) {
        Intent intent = new Intent(context, ForecastDetailActivity.class);
        intent.putExtra(EXTRA_CITY_ID, cityId);
        intent.putExtra(EXTRA_CITY_NAME, cityName);
        intent.putExtra(EXTRA_DAY, day);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast_detail);
        setSupportActionBar((Toolbar) findViewById(R.id.toolBar));
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String cityName = getIntent().getStringExtra(EXTRA_CITY_NAME);
        getSupportActionBar().setTitle(cityName + " Forecast");

        long cityId = getIntent().getLongExtra(EXTRA_CITY_ID, 0);
        long day = getIntent().getLongExtra(EXTRA_DAY, 0);

        ForecastFragment forecastFragment = (ForecastFragment)
                getSupportFragmentManager().findFragmentById(R.id.forecast_fragment);
        forecastFragment.setCityIdAndDay(cityId, day);
    }

}

package com.example.danco.homework2.service;


import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import com.example.danco.homework2.network.CacheUtils;
import com.example.danco.homework2.network.ConditionsJsonParser;
import com.example.danco.homework2.network.ConnectivityUtils;
import com.example.danco.homework2.network.DataFetcher;
import com.example.danco.homework2.provider.CityConditionsContract;

import java.net.URL;

class FetchConditionsHelper {

    private static final String TAG = "FetchConditionsHelper";
    private static final String WHERE_CONTENT_STALE =
            CityConditionsContract.Columns.UPDATED + " < ?";
    private static final long TWO_HOURS = 2 * 60 * 60 * 1000;


    public interface DelayAndRetryHandler {
        public void delayAndRetry();

        public void delayAndRetryError();

        public boolean isLoadCancelled();
    }


    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    public static void loadData(Context context, DelayAndRetryHandler handler) {
        if (!DataFetcher.ensureLatestSSL(context)) {
            return;
        }

        CacheUtils.initializeCache(context);

        // No network, we should get out
        if (ConnectivityUtils.isNotConnected(context)) {
            handler.delayAndRetryError();
            return;
        }

        try {
            URL url = WeatherService.buildURL(new String[]{"weather"}, 5809844L);
            if (handler.isLoadCancelled()) {
                return;
            }
            ContentValues[] values = DataFetcher.performGet(url, new ConditionsJsonParser());
            if (values != null) {
                if (handler.isLoadCancelled()) {
                    return;
                }
                int rows = context.getContentResolver().bulkInsert(CityConditionsContract.URI,
                        values);
                Log.d(TAG, "Inserted rows: " + rows);

                if (handler.isLoadCancelled()) {
                    return;
                }
            }
        } catch (Exception e) {
            Log.w(TAG, "Unexpected error", e);
        }

        CacheUtils.logCache();
    }
}

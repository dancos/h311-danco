package com.example.danco.homework2.fragment;


import android.content.IntentSender;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.danco.BuildConfig;
import com.example.danco.R;
import com.example.danco.homework2.loader.CityConditionsLoaderCallbacks;
import com.example.danco.homework2.provider.CityConditionsContract;
import com.example.danco.homework2.receiver.LocationBroadcastReceiver;
import com.example.danco.homework2.service.WeatherService;
import com.example.danco.homework2.utils.LocationUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConditionsFragment extends Fragment
        implements CityConditionsLoaderCallbacks.OnCityConditionsLoaded,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        PlayServicesDialogFragment.PlayServicesDialogFragmentListener,
        ResultCallback<LocationSettingsResult>,
        LocationListener {

    private static final String[] PROJECTION = new String[]{
            CityConditionsContract.Columns.ICON,
            CityConditionsContract.Columns.CONDITION,
            CityConditionsContract.Columns.TEMPERATURE,
            CityConditionsContract.Columns.HUMIDITY,
            CityConditionsContract.Columns.PRESSURE,
            CityConditionsContract.Columns.WIND_DIRECTION,
            CityConditionsContract.Columns.WIND_SPEED,
            CityConditionsContract.Columns.CITY_ID,
            CityConditionsContract.Columns.CITY_NAME
    };

    private static final int ICON_POS = 0;
    private static final int CONDITION_POS = 1;
    private static final int TEMPERATURE_POS = 2;
    private static final int HUMIDITY_POS = 3;
    private static final int PRESSURE_POS = 4;
    private static final int WIND_DIRECTION_POS = 5;
    private static final int WIND_SPEED_POS = 6;
    private static final int CITY_ID_POS = 7;
    private static final int CITY_NAME_POS = 8;

    private static final String LOG_TAG = ConditionsFragment.class.getSimpleName();

    private static final String STATE_RESOLVING_ERROR = "resolvingError";
    private static final String GPS_DIALOG_TAG = "GooglePlayServiceDialog";

    private static final int RESOLVE_ERROR_REQUEST = 110;
    public static final int CHECK_SETTINGS_REQUEST = 115;

    private static final long TEN_SECONDS = 10 * 1000;
    private static final long ONE_MINUTE = 60 * 1000;
    private static final long ONE_HOUR = 60 * ONE_MINUTE;
    private static final float MIN_DISTANCE = 250; // meters
    public static final String CITY_ID = "city_id";
    public static final String CITY_NAME = "city_name";

    private boolean isCreated = false;
    private String cityName;
    private long cityId;

    private CityChangedListener listener;
    private GoogleApiClient googleApiClient;
    private Location lastLocation;
    private LocationRequest locationRequest;
    private LocationRequest backgroundLocationRequest;
    private boolean resolvingError;



    public ConditionsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        lastLocation = LocationUtils.getLastLocation(getActivity());

        resolvingError = savedInstanceState != null &&
                savedInstanceState.getBoolean(STATE_RESOLVING_ERROR);

        googleApiClient = new GoogleApiClient
                .Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();


        // First built the request the fragment will be using to request updates.
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setSmallestDisplacement(MIN_DISTANCE);

        // For this app we are only using location to filter 911 events in roughly 1 km rectangle
        // around your current location.

        // How frequent you want to be informed, the slower the better even if you want GPS.
        locationRequest.setInterval(ONE_MINUTE);

        // If another app requests faster updates, what is the max rate you want to be informed
        locationRequest.setFastestInterval(TEN_SECONDS);

        // Max time you are willing to wait for update, ideally double your interval. This lets
        // the fused location manager schedule your app with others to reduce battery use, etc
        locationRequest.setExpirationTime(2 * ONE_MINUTE);

        // If only want a limited number of updates. Essentially self cancels when done.
        //locationRequest.setNumUpdates(1);

        backgroundLocationRequest = new LocationRequest();
        backgroundLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
        backgroundLocationRequest.setSmallestDisplacement(MIN_DISTANCE);
        backgroundLocationRequest.setInterval(TEN_SECONDS);
        backgroundLocationRequest.setExpirationTime(2 * ONE_HOUR);
        // Only want one background request.
        backgroundLocationRequest.setNumUpdates(1);
    }

    @Override
    public void onStart() {
        super.onStart();

        // If resolving an error, do not start the client... as it is already started.
        if (!resolvingError) {
            googleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        // Switch to background mode. Not a typical way of requesting background updates, but
        // for a quick example will do. Remember the backgroundLocationRequest only wants one
        // update so it will shutdown eventually.
        LocationServices.FusedLocationApi.requestLocationUpdates(
                googleApiClient,
                backgroundLocationRequest,
                LocationBroadcastReceiver.buildPendingIntent(getActivity()));
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(STATE_RESOLVING_ERROR, resolvingError);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        CityConditionsLoaderCallbacks.initLoader(getLoaderManager(), getActivity(), this,
                PROJECTION, cityId);
        isCreated = true;
    }


    @Override
    public void onCityConditionsLoaded(Cursor cursor) {
        if (cursor == null) return;
        ViewHolder holder = getViewHolder();
        if (holder != null && cursor.moveToFirst()) {
            holder.description.setText(R.string.current_conditions);
            Picasso.with(getActivity())
                    .load(WeatherService.buildIconUri(cursor.getString(ICON_POS)))
                    .noFade()
                    .into(holder.icon);
            holder.temp.setText(getString(R.string.temperature,
                    Math.round(cursor.getDouble(TEMPERATURE_POS))));
            holder.condition.setText(cursor.getString(CONDITION_POS));
            holder.humidity.setText(getString(R.string.humidity, cursor.getInt(HUMIDITY_POS)));
            holder.pressure.setText(getString(R.string.pressure, cursor.getDouble(PRESSURE_POS)));
            holder.wind.setText(getString(R.string.wind,
                    cursor.getString(WIND_DIRECTION_POS),
                    cursor.getDouble(WIND_SPEED_POS)));
            cityName = cursor.getString(CITY_NAME_POS);
            cityId = cursor.getLong(CITY_ID_POS);
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        // Connected to Google Play services!
        // The good stuff goes here, if you are able to do things that are outside of UI
        // interaction. For places, it depends on search so this app has nothing to do.
        Log.d(LOG_TAG, "Connected");

        // Lets see if we have location services enabled...


        // Ensure location service settings are enabled for what we need. If you have different
        // location requests, you may check both at same time
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient,
                        builder.build());

        result.setResultCallback(this);
    }


    @Override
    public void onConnectionSuspended(int cause) {
        // Lost connection.
        // Good place to cleanup or suspect any background ops that depend on the connection.
        // Not sure what more you could do...
        // BTW the googleApiClient has isConnected()/isConnecting() methods so no need to track.
        Log.d(LOG_TAG, "Connection suspended: " + cause);
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (resolvingError) {
            // Already attempting to resolve an error.
            return;
        }

        if (connectionResult.hasResolution()) {
            try {
                resolvingError = true;
                connectionResult.startResolutionForResult(getActivity(), RESOLVE_ERROR_REQUEST);
            }
            catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                googleApiClient.connect();
                resolvingError = false;
            }
        }
        else {
            // Show dialog using GooglePlayServicesUtil.getErrorDialog()
            // Must use your own fragment wrapper around the dialog or rotation does not work correctly.
            // Luckily you could essentially copy the PlayServicesDialogFragment from project to project.
            // Works with both activities/fragments as parent. However, the onActivityResult() is always
            // routed to the "Activity". So the activity is responsible for notifying any fragments that
            // google play services are available.
            DialogFragment fragment = PlayServicesDialogFragment.newInstance(
                    connectionResult.getErrorCode());
            fragment.show(getChildFragmentManager(), GPS_DIALOG_TAG);
            resolvingError = true;
        }
    }


    @Override
    public void onDialogCancelled() {
        Log.d(LOG_TAG, "Dialog cancelled");
//        listener.onPlayServicesUnavailable();
    }


    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                requestLocationUpdates();
                return;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                // Location settings are not satisfied. But could be fixed by showing the user
                // a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    status.startResolutionForResult(getActivity(), CHECK_SETTINGS_REQUEST);
                    return;
                }
                catch (IntentSender.SendIntentException e) {
                    // Ignore the error. Nothing we can do...
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are not satisfied. However, we have no way to fix the
                // settings so we won't show the dialog...
                break;
        }

        // Must be in an error state, fallback to last known location
        requestLastKnownLocation();
    }


    @Override
    public void onLocationChanged(Location location) {
        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, "onLocationChanged: " + location);
        }
        if (LocationUtils.isBetterLocation(location, lastLocation)) {
            updateLocation(location);
        }
    }

    private void requestLocationUpdates() {

        // First refresh using last known location so user doesn't wait for location request.
        requestLastKnownLocation();

        // Request updates using our location request criteria
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest,
                this);
    }

    private void requestLastKnownLocation() {
        Location newLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (LocationUtils.isBetterLocation(newLocation, lastLocation)) {
            if (BuildConfig.DEBUG) {
                Log.d(LOG_TAG, "Using last location: " + newLocation);
            }
            updateLocation(newLocation);
        }
        else if (lastLocation != null) {
            if (BuildConfig.DEBUG) {
                Log.d(LOG_TAG, "Using last saved location: " + lastLocation);
            }
            updateLocation(lastLocation);
        }
    }

    private void updateLocation(Location location) {
//        listener.onCityChanged(cityName, cityId);
        lastLocation = location;
        LocationUtils.saveLastLocation(getActivity(), location);
    }


    public void setCityId(long cityId) {
        this.cityId = cityId;
        if (isCreated) {
            CityConditionsLoaderCallbacks.restartLoader(getLoaderManager(), getActivity(), this,
                    PROJECTION, cityId);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_conditions, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ViewHolder holder = new ViewHolder(view);
        view.setTag(holder);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }


    @Nullable
    protected ViewHolder getViewHolder() {
        View view = getView();
        return view != null ? (ViewHolder) view.getTag() : null;
    }


    protected static class ViewHolder {
        final TextView description;
        final ImageView icon;
        final TextView temp;
        final TextView condition;
        final TextView wind;
        final TextView pressure;
        final TextView humidity;


        ViewHolder(View view) {
            description = (TextView) view.findViewById(R.id.description);
            icon = (ImageView) view.findViewById(R.id.icon);
            temp = (TextView) view.findViewById(R.id.temperature);
            condition = (TextView) view.findViewById(R.id.condition);
            wind = (TextView) view.findViewById(R.id.wind);
            pressure = (TextView) view.findViewById(R.id.pressure);
            humidity = (TextView) view.findViewById(R.id.humidity);
        }
    }
}

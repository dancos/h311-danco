package com.example.danco.homework2.service;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.example.danco.BuildConfig;
import com.example.danco.homework2.network.CacheUtils;
import com.example.danco.homework2.network.ConditionsJsonParser;
import com.example.danco.homework2.network.DailyForecastJsonParser;
import com.example.danco.homework2.network.DataFetcher;
import com.example.danco.homework2.network.ForecastJsonParser;
import com.example.danco.homework2.provider.BaseContract;
import com.example.danco.homework2.provider.CityConditionsContract;
import com.example.danco.homework2.provider.DailyForecastContract;
import com.example.danco.homework2.provider.ForecastContract;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class WeatherService extends IntentService {
    private static final String TAG = "WeatherService";

    private static final String ACTION_RELOAD_WEATHER =
            BuildConfig.APPLICATION_ID + ".service.action.RELOAD_WEATHER";
    private static final String ACTION_LOAD_WEATHER =
            BuildConfig.APPLICATION_ID + ".service.action.LOAD_WEATHER";
    private static final String ACTION_LOAD_FORECAST =
            BuildConfig.APPLICATION_ID + ".service.action.LOAD_FORECAST";
    private static final String ACTION_LOAD_DAILY_FORECAST =
            BuildConfig.APPLICATION_ID + ".service.action.LOAD_DAILY_FORECAST";

    private static final String ACTION_LOAD_WEATHER_WAKEFUL =
            BuildConfig.APPLICATION_ID + ".service.action.LOAD_WEATHER_WAKEFUL";
    private static final String ACTION_LOAD_FORECAST_WAKEFUL =
            BuildConfig.APPLICATION_ID + ".service.action.LOAD_FORECAST_WAKEFUL";
    private static final String ACTION_LOAD_DAILY_FORECAST_WAKEFUL =
            BuildConfig.APPLICATION_ID + ".service.action.LOAD_DAILY_FORECAST_WAKEFUL";

    private static final String EXTRA_BACKOFF = WeatherService.class.getName() + ".extra.backoff";

    private static final String EXTRA_CITY_ID = "cityId";
    private static final String EXTRA_CITY_NAME = "cityName";

    private static final String[] AGE_PROJECTION =
            new String[]{"max(" + BaseContract.BaseWeatherColumns.UPDATED + ")"};
    private static final String WHERE_CITY_ID_MATCHES =
            BaseContract.BaseWeatherColumns.CITY_ID + " = ?";

    private static final long TEN_MINUTES = 10 * 60 * 1000;
    private static final long SIX_HOURS = 6 * 60 * 60 * 1000;

    private static final boolean FORCE_RELOAD = true;


    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionLoad(Context context, long cityId) {
        Intent intent = new Intent(context, WeatherService.class);
        intent.setAction(ACTION_LOAD_WEATHER);
        intent.putExtra(EXTRA_CITY_ID, cityId);
        context.startService(intent);
    }


    public static void startActionReLoad(Context context, long cityId, String cityName) {
        Intent intent = new Intent(context, WeatherService.class);
        intent.setAction(ACTION_RELOAD_WEATHER);
        intent.putExtra(EXTRA_CITY_ID, cityId);
        intent.putExtra(EXTRA_CITY_NAME, cityName);
        context.startService(intent);
    }


    public static void startActionLoadForecast(Context context, long cityId) {
        Intent intent = new Intent(context, WeatherService.class);
        intent.setAction(ACTION_LOAD_FORECAST);
        intent.putExtra(EXTRA_CITY_ID, cityId);
        context.startService(intent);
    }


    public static void startActionLoadDailyForecast(Context context, long cityId) {
        Intent intent = new Intent(context, WeatherService.class);
        intent.setAction(ACTION_LOAD_DAILY_FORECAST);
        intent.putExtra(EXTRA_CITY_ID, cityId);
        context.startService(intent);
    }


    public static Intent buildWakefulWeatherIntent(Context context, long backoff) {
        Intent intent = new Intent(context, WeatherService.class);
        intent.setAction(ACTION_LOAD_WEATHER_WAKEFUL);
        intent.putExtra(EXTRA_BACKOFF, backoff);
        return intent;
    }


    public WeatherService() {
        super("WeatherService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            long cityId = intent.getLongExtra(EXTRA_CITY_ID, 0);
            switch (action) {
                case ACTION_RELOAD_WEATHER:
                    handleActionLoadWeather(cityId, FORCE_RELOAD);
                    break;
                case ACTION_LOAD_WEATHER:
                    handleActionLoadWeather(cityId);
                    break;
                case ACTION_LOAD_FORECAST:
                    handleActionLoadForecast(cityId);
                    break;
                case ACTION_LOAD_DAILY_FORECAST:
                    handleActionLoadDailyForecast(cityId);
                    break;
                case ACTION_LOAD_WEATHER_WAKEFUL:
                    try {
                        handleActionLoadWeather(cityId);
                    } finally {
                        WakefulBroadcastReceiver.completeWakefulIntent(intent);
                    }
                default:
                    break;
            }
        }
    }


    private void handleActionLoadWeather(long cityId) {
        handleActionLoadWeather(cityId, false);
    }


    private void handleActionLoadWeather(long cityId, boolean force) {
        CacheUtils.initializeCache(this);

        if (!DataFetcher.ensureLatestSSL(this)) {
            return;
        }

        // Only refresh conditions every ten minutes unless forced
        if (!force && dataValid(CityConditionsContract.URI, TEN_MINUTES, null, null)) {
            return;
        }

        URL url = buildURL(new String[]{"weather"}, cityId);
        ContentValues[] values = DataFetcher.performGet(url, new ConditionsJsonParser());
        if (values != null) {
            getContentResolver().bulkInsert(CityConditionsContract.URI, values);

            // Queue up forecast work
            for (ContentValues data : values) {
//                long cityId = data.getAsLong(CityConditionsContract.Columns.CITY_ID);
                String[] selectionArgs = new String[]{Long.toString(cityId)};

                // Update daily forecast every six hours. Not changing much...
                if (force || !dataValid(DailyForecastContract.URI, SIX_HOURS,
                        WHERE_CITY_ID_MATCHES, selectionArgs)) {
                    startActionLoadDailyForecast(this, cityId);
                }

                // Update forecast every six hours to maintain future periods...
                if (force || !dataValid(ForecastContract.URI, SIX_HOURS,
                        WHERE_CITY_ID_MATCHES, selectionArgs)) {
                    startActionLoadForecast(this, cityId);
                }
            }
        }

        if (BuildConfig.DEBUG) {
            CacheUtils.logCache();
        }
    }


    private void handleActionLoadForecast(long cityId) {
        CacheUtils.initializeCache(this);

        if (!DataFetcher.ensureLatestSSL(this)) {
            return;
        }

        URL url = buildURL(new String[]{"forecast"}, cityId);
        ContentValues[] values = DataFetcher.performGet(url, new ForecastJsonParser());
        int rows = 0;
        if (values != null) {
            rows = getContentResolver().bulkInsert(ForecastContract.URI, values);
        }

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Inserted " + rows + " Forecast Rows for city: " + cityId);
            CacheUtils.logCache();
        }
    }


    private void handleActionLoadDailyForecast(long cityId) {
        CacheUtils.initializeCache(this);

        if (!DataFetcher.ensureLatestSSL(this)) {
            return;
        }

        URL url = buildURL(new String[]{"forecast", "daily"}, cityId);
        ContentValues[] values = DataFetcher.performGet(url, new DailyForecastJsonParser());
        int rows = 0;
        if (values != null) {
            rows = getContentResolver().bulkInsert(DailyForecastContract.URI, values);

        }

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Inserted " + rows + " Daily Forecast Rows for city: " + cityId);
            CacheUtils.logCache();
        }
    }


    /* package */
    static URL buildURL(String[] paths, long cityId) {
        if (cityId == 0) {
            Log.w(TAG, "Bad city id");
        }

        Uri.Builder builder = new Uri.Builder()
                .scheme("http")
                .authority(CitySelectorJobService.WEATHER_API_PROVIDER)
                .appendPath("data")
                .appendPath("2.5");
        for (String path : paths) {
            builder.appendPath(path);
        }
        builder.appendQueryParameter("id", Long.toString(cityId))
                .appendQueryParameter("units", "imperial")
                .appendQueryParameter("APPID", BuildConfig.APP_ID);

        Uri uri = builder.build();

        if (BuildConfig.DEBUG) {
            Log.d(TAG, uri.toString());
        }
        try {
            return new URL(uri.toString());
        } catch (IOException e) {
            Log.w(TAG, "Failed to build URL: " + uri.toString());
            return null;
        }
    }


    public static Uri buildIconUri(String iconTarget) {
        Uri.Builder builder = new Uri.Builder()
                .scheme("http")
                .authority(CitySelectorJobService.WEATHER_API_PROVIDER)
                .appendPath("img")
                .appendPath("w")
                .appendPath(iconTarget + ".png");

        Uri uri = builder.build();

        return uri;
    }


    private boolean dataValid(Uri uri, long ageMillis, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        boolean isDataValid = false;

        try {
            cursor = getContentResolver()
                    .query(uri, AGE_PROJECTION, selection, selectionArgs, null);
            if (cursor.moveToFirst()) {
                long updateTime = cursor.isNull(0) ? 0 : cursor.getLong(0);
                isDataValid = updateTime > System.currentTimeMillis() - ageMillis;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return isDataValid;
    }
}

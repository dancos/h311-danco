package com.example.danco.homework2.adapter;

import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;


public class PlaceItem {
    final private String description;
    final private String placeId;
    private LatLng latLng;


    public PlaceItem(AutocompletePrediction prediction) {
        description = prediction.getDescription();
        placeId = prediction.getPlaceId();
    }


    public String getDescription() {
        return description;
    }


    public String getPlaceId() {
        return placeId;
    }


    public static ArrayList<PlaceItem> buildPlaceItemList(AutocompletePredictionBuffer buffer) {
        int count = buffer.getCount();
        ArrayList<PlaceItem> result = new ArrayList<>(count);
        for (int i = 0; i < count; ++i) {
            result.add(new PlaceItem(buffer.get(i)));
        }
        return result;
    }


    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }


    public LatLng getLatLng() {
        return latLng;
    }
}

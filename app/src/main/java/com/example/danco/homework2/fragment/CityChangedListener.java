package com.example.danco.homework2.fragment;

/**
 * @author danco on 5/23/15.
 */
public interface CityChangedListener {
    void onCityChanged(String cityName, long cityId);
}

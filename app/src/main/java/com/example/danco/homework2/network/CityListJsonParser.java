package com.example.danco.homework2.network;

import android.content.ContentValues;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.JsonReader;

import com.example.danco.homework2.provider.CityConditionsContract;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author danco on 5/17/15.
 */
public class CityListJsonParser implements DataFetcher.DataConverter {

    private static final String LOG_TAG = CityListJsonParser.class.getSimpleName();


    @Nullable
    @Override
    public ContentValues[] convertData(@NonNull InputStream inputStream) {

        JsonReader jsonReader = null;
        try {
            jsonReader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
            return new ContentValues[]{processListObject(jsonReader)};
        } catch (IOException e) {
            CommonJsonParser.logIOException(LOG_TAG, e);
            return null;
        } finally {
            CommonJsonParser.closeReader(LOG_TAG, jsonReader);
        }
    }


    /* package */
    static ContentValues processListObject(JsonReader jsonReader) throws IOException {
        ContentValues values = new ContentValues();

        jsonReader.beginObject();

        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();

            switch (name) {
                case JsonConstants.Forecast.LIST:
                    processCity(jsonReader, values);
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }

        jsonReader.endObject();
        return values;
    }


    private static void processCity(JsonReader jsonReader, ContentValues values)
            throws IOException {

        jsonReader.beginArray();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();

            switch (name) {
                case JsonConstants.Conditions.CITY_ID:
                    values.put(CityConditionsContract.Columns.CITY_ID, jsonReader.nextLong());
                    break;
                case JsonConstants.Conditions.CITY_NAME:
                    values.put(CityConditionsContract.Columns.CITY_NAME, jsonReader.nextString());
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }

        jsonReader.endObject();
        jsonReader.endArray();
    }
}

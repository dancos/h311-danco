package com.example.danco.homework2.network;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.danco.homework2.service.CityConditionsJobService;
import com.example.danco.homework2.service.CitySelectorJobService;
import com.example.danco.homework2.service.DailyForecastJobService;
import com.example.danco.homework2.service.ForecastJobService;
import com.google.android.gms.maps.model.LatLng;


public class JobUtils {

    private static final String TAG = JobUtils.class.getSimpleName();

    private static final int CITY_CONDITIONS_ID = 100;
    private static final int DAILY_FORECAST_ID = 200;
    private static final int FORECAST_ID = 300;

    private static final long FOUR_MINUTES = 4 * 60 * 1000;
    private static final long SIX_HOURS = 6 * 60 * 60 * 1000;
    private static final long JOB_INTERVAL = SIX_HOURS;
    private static final String LATITUDE = "lat";
    private static final String LONGITUDE = "lng";


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void scheduleCityConditionsUpdate(Context context) {
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(
                Context.JOB_SCHEDULER_SERVICE);

        // Safe to reschedule jobs... New job replaces old one.
        JobInfo newJob = buildJobInfo(context, CityConditionsJobService.class);
        int result = jobScheduler.schedule(newJob);
        if (result == JobScheduler.RESULT_FAILURE) {
            Log.w(TAG, "Job failed to be scheduled");
        } else {
            Log.d(TAG, "Job scheduled");
        }
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void scheduleDailyForecastUpdate(Context context) {
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(
                Context.JOB_SCHEDULER_SERVICE);

        // Safe to reschedule jobs... New job replaces old one.
        JobInfo newJob = buildJobInfo(context, DailyForecastJobService.class);
        int result = jobScheduler.schedule(newJob);
        if (result == JobScheduler.RESULT_FAILURE) {
            Log.w(TAG, "Job failed to be scheduled");
        } else {
            Log.d(TAG, "Job scheduled");
        }
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void scheduleForecastUpdate(Context context) {
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(
                Context.JOB_SCHEDULER_SERVICE);

        // Safe to reschedule jobs... New job replaces old one.
        JobInfo newJob = buildJobInfo(context, ForecastJobService.class);
        int result = jobScheduler.schedule(newJob);
        if (result == JobScheduler.RESULT_FAILURE) {
            Log.w(TAG, "Job failed to be scheduled");
        } else {
            Log.d(TAG, "Job scheduled");
        }
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void scheduleCitySelectionUpdate(Context context, LatLng latlng) {
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(
                Context.JOB_SCHEDULER_SERVICE);

        // Safe to reschedule jobs... New job replaces old one.
        PersistableBundle extras = new PersistableBundle(2);
        extras.putDouble(LATITUDE, latlng.latitude);
        extras.putDouble(LONGITUDE, latlng.longitude);
        JobInfo newJob = buildJobInfo(context, CitySelectorJobService.class, extras);
        int result = jobScheduler.schedule(newJob);
        if (result == JobScheduler.RESULT_FAILURE) {
            Log.w(TAG, "Job failed to be scheduled");
        } else {
            Log.d(TAG, "Job scheduled");
        }
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static JobInfo buildJobInfo(Context context, Class clazz) {
        JobInfo.Builder builder = new JobInfo.Builder(CITY_CONDITIONS_ID,
                new ComponentName(context, clazz))

                // Want to run periodically. Other options are for one time runs, etc
                .setPeriodic(JOB_INTERVAL)

                        // Indicate want job to continue to run across a reboot of device. Requires
                        // RECEIVE_BOOT_COMPLETED permission
                .setPersisted(true)

                        // Only backing off if get HTTP_ACCEPTED response indicating app should
                        // retry fetch otherwise, wait for next scheduled event.
                .setBackoffCriteria(500, JobInfo.BACKOFF_POLICY_EXPONENTIAL)

                        // Only want job to run if have un metered network access. Unmetered is
                        // suitable for large downloads without incurring cost to user. This does
                        // not mean it is Wifi. Therefore if you want to restrict to wifi, you
                        // likely have to do an additional check on job run.
                        // http://developer.android.com/reference/android/net/ConnectivityManager.html#isActiveNetworkMetered()
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)

                        // Indicate if you only want to be woken up if the device is charging.
                .setRequiresCharging(false);

                        // Advanced apps would add settings for interval and WIFI only... to allow
                        // user to control how often this is done and under what conditions.

        return builder.build();
    }



    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static JobInfo buildJobInfo(Context context, Class clazz,
                                        @Nullable PersistableBundle extras) {
        JobInfo.Builder builder = new JobInfo.Builder(CITY_CONDITIONS_ID,
                new ComponentName(context, clazz))
                .setMinimumLatency(1000)
                // Only want job to run if have un metered network access. Unmetered is
                // suitable for large downloads without incurring cost to user. This does
                // not mean it is Wifi. Therefore if you want to restrict to wifi, you
                // likely have to do an additional check on job run.
                // http://developer.android.com/reference/android/net/ConnectivityManager.html#isActiveNetworkMetered()
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)

                // Indicate if you only want to be woken up if the device is charging.
                .setRequiresCharging(false);

        if (extras != null) {
            builder.setExtras(extras);
        }

        return builder.build();
    }
}

package com.example.danco.homework2.application;

import android.app.Application;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.example.danco.BuildConfig;
import com.example.danco.homework2.activity.WeatherActivity;
import com.example.danco.homework2.fragment.PlayServicesDialogFragment;
import com.example.danco.homework2.service.CitySelectorService;
import com.example.danco.homework2.utils.LocationUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.squareup.otto.Bus;

/**
 * @author danco on 5/24/15.
 */
public class WeatherApplication extends Application implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        PlayServicesDialogFragment.PlayServicesDialogFragmentListener,
        ResultCallback<LocationSettingsResult>,
        LocationListener {
    public static final String CITY_ID = "city_id";
    public static final String CITY_NAME = "city_name";

    private static final String LOG_TAG = WeatherApplication.class.getSimpleName();
    private static final long TEN_SECONDS = 10 * 1000;
    private static final long ONE_MINUTE = 60 * 1000;
    private static final long ONE_HOUR = 60 * ONE_MINUTE;
    private static final float MIN_DISTANCE = 250; // meters

    private GoogleApiClient googleApiClient;
    private Location lastLocation;
    private LocationRequest locationRequest;
    private LocationRequest backgroundLocationRequest;

    private String cityName;
    private long cityId;

    private Bus bus;

    /**
     * Called when the application is starting, before any activity, service,
     * or receiver objects (excluding content providers) have been created.
     * Implementations should be as quick as possible (for example using
     * lazy initialization of state) since the time spent in this function
     * directly impacts the performance of starting the first activity,
     * service, or receiver in a process.
     * If you override this method, be sure to call super.onCreate().
     */
    @Override
    public void onCreate() {
        super.onCreate();

        bus = new Bus();

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        // First built the request the fragment will be using to request updates.
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setSmallestDisplacement(MIN_DISTANCE);

        // For this app we are only using location to filter 911 events in roughly 1 km rectangle
        // around your current location.

        // How frequent you want to be informed, the slower the better even if you want GPS.
        locationRequest.setInterval(ONE_MINUTE);

        // If another app requests faster updates, what is the max rate you want to be informed
        locationRequest.setFastestInterval(TEN_SECONDS);

        // Max time you are willing to wait for update, ideally double your interval. This lets
        // the fused location manager schedule your app with others to reduce battery use, etc
        locationRequest.setExpirationTime(2 * ONE_MINUTE);

        // If only want a limited number of updates. Essentially self cancels when done.
        //locationRequest.setNumUpdates(1);

        backgroundLocationRequest = new LocationRequest();
        backgroundLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
        backgroundLocationRequest.setSmallestDisplacement(MIN_DISTANCE);
        backgroundLocationRequest.setInterval(TEN_SECONDS);
        backgroundLocationRequest.setExpirationTime(2 * ONE_HOUR);
        // Only want one background request.
        backgroundLocationRequest.setNumUpdates(1);

        // If resolving an error, do not start the client... as it is already started.
        googleApiClient.connect();
    }


    @Override
    public void onConnected(Bundle bundle) {
        lastLocation = LocationServices.FusedLocationApi.getLastLocation(
                googleApiClient);
        if (lastLocation != null) {
            CitySelectorService.startActionLoadCity(getApplicationContext(),
                    lastLocation.getLatitude(), lastLocation.getLongitude());
        }
    }


    @Override
    public void onConnectionSuspended(int i) {

    }


    @Override
    public void onLocationChanged(Location location) {
        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, "onLocationChanged: " + location);
        }
        if (LocationUtils.isBetterLocation(location, lastLocation)) {
            updateLocation(location);
        }
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    @Override
    public void onDialogCancelled() {

    }


    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                requestLocationUpdates();
                return;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                // Location settings are not satisfied. But could be fixed by showing the user
                // a dialog.
//                try {
//                    // Show the dialog by calling startResolutionForResult(),
//                    // and check the result in onActivityResult().
//                    status.startResolutionForResult(getActivity(), CHECK_SETTINGS_REQUEST);
//                    return;
//                }
//                catch (IntentSender.SendIntentException e) {
//                    // Ignore the error. Nothing we can do...
//                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are not satisfied. However, we have no way to fix the
                // settings so we won't show the dialog...
                break;
        }

        // Must be in an error state, fallback to last known location
        requestLastKnownLocation();
    }


    private void requestLocationUpdates() {

        // First refresh using last known location so user doesn't wait for location request.
        requestLastKnownLocation();

        // Request updates using our location request criteria
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest,
                this);
    }

    private void requestLastKnownLocation() {
        Location newLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (LocationUtils.isBetterLocation(newLocation, lastLocation)) {
            if (BuildConfig.DEBUG) {
                Log.d(LOG_TAG, "Using last location: " + newLocation);
            }
            updateLocation(newLocation);
        }
        else if (lastLocation != null) {
            if (BuildConfig.DEBUG) {
                Log.d(LOG_TAG, "Using last saved location: " + lastLocation);
            }
            updateLocation(lastLocation);
        }
    }

    private void updateLocation(Location location) {
        lastLocation = location;
        LocationUtils.saveLastLocation(getApplicationContext(), location);
    }


    public Location getLastLocation() {
        return lastLocation;
    }


    public long getCityId() {
        return cityId;
    }


    public String getCityName() {
        return cityName;
    }


    public Bus getBus() {
        return bus;
    }
}

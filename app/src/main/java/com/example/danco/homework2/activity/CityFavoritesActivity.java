package com.example.danco.homework2.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.provider.BaseColumns;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.danco.R;
import com.example.danco.homework2.fragment.CityFavoritesFragment;
import com.example.danco.homework2.provider.CityConditionsContract;
import com.example.danco.homework2.provider.CityContract;
import com.example.danco.homework2.provider.DailyForecastContract;
import com.example.danco.homework2.provider.ForecastContract;
import com.example.danco.homework2.service.WeatherService;

public class CityFavoritesActivity extends AppCompatActivity implements
        CityFavoritesFragment.CityFavoritesFragmentListener {
    private static final String LOG_TAG = CityFavoritesActivity.class.getSimpleName();

    private static final String CITY_FAVORITES_FRAG = "CityFragment";
    private static final String EXTRA_CITY_NAME = "cityName";
    private static final String EXTRA_CITY_ID = "cityId";

    private static final int ADD_FAVORITE_CODE = 200;

    private static final String WHERE_MATCHES_ID = BaseColumns._ID + " = ?";
    private static final String WHERE_MATCHES_CITY_ID =
            CityConditionsContract.Columns.CITY_ID + " = ?";


    public static Intent buildIntent(Context context) {
        return new Intent(context, CityFavoritesActivity.class);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_favorites);
        setSupportActionBar((Toolbar) findViewById(R.id.toolBar));
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.favorites_container, CityFavoritesFragment.newInstance(),
                            CITY_FAVORITES_FRAG)
                    .commit();
        }
    }


    @Override
    public void onFavoriteSelected(String cityName, long cityId) {
        //restart loader for selected cityId and navigate up to parent (WeatherActivity)
        WeatherService.startActionReLoad(getApplicationContext(), cityId, cityName);

        Intent data = new Intent();
        data.putExtra(EXTRA_CITY_NAME, cityName);
        data.putExtra(EXTRA_CITY_ID, cityId);
        setResult(Activity.RESULT_OK, data);
        finish();
    }


    @Override
    public void onFavoriteLongClick(String cityName, long cityId, long id) {
        Log.i(LOG_TAG, "delete favorite city: " + cityName);
        // it's easy enough to add a city back if you delete by accident, so I like
        // not having a confirmation dialog
        getContentResolver().delete(CityContract.URI, WHERE_MATCHES_ID,
                new String[]{Long.toString(id)});
        getContentResolver().delete(CityConditionsContract.URI, WHERE_MATCHES_CITY_ID,
                new String[]{Long.toString(cityId)});
        getContentResolver().delete(ForecastContract.URI, WHERE_MATCHES_CITY_ID,
                new String[]{Long.toString(cityId)});
        getContentResolver().delete(DailyForecastContract.URI, WHERE_MATCHES_CITY_ID,
                new String[]{Long.toString(cityId)});
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_city_favorites, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //noinspection SimplifiableIfStatement
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                Intent intent = NavUtils.getParentActivityIntent(this);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                NavUtils.navigateUpTo(this, intent);
                return true;

            case R.id.action_choose_city:
                startActivityForResult(CityChooserActivity.buildIntent(this), ADD_FAVORITE_CODE);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
